# Repozytorium uzupełniające szkolenie z języka Csharp

## Spotkanie 1 - **Podstawy języka C#**

1. Wstęp
2. Środowisko i ogólne informacje o języku
3. Składnia
4. Typy
5. Łańcuchy
6. Tablice
7. Zmienne
8. Wyrażenia
9. Instrukcje warunkowe i pętle
10. Przestrzenie - zajawienie tematu

## Spotkanie 2 - **Więcej o typach**

1. Klasy i dziedziczenie
2. Object
3. Struct
4. Modyfikatory
5. Interfejsy
6. Enumeracje
7. Generics
8. Kolekcje - zajawienie tematu

## Spotkanie 3 - **Metody programowania**

1. Single Responsibility Principle
2. Open/Closed Principle
3. Liskov Substitution Principle
4. Interface Segregation Principle
5. Dependency Inversion Principle
6. Metaprinciples (KISS, YAGNI, DRY ..)
7. Comparison SOLID vs Metaprinciples
