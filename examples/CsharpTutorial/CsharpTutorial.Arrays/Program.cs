﻿using System;

namespace CsharpTutorial.Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tablica określa konkretną liczbę elementów danego typu, każda tablica dziedziczy po klasie System.Array
            char[] characters = new char[10]; // deklaracja dla 10 elementowej tablicy z typem char, każda wartość jest defaultowo inicjalizowana
            characters[0] = 'x';
            characters[1] = 'y';
            // jeśli odwołamy się do nieistniejącego indeksu zostanie rzucony wyjątek index out of range
            for (int i = 0; i < characters.Length; i++) // pętla for dla całej tablicy characters
            {
                Console.WriteLine(characters[i]); 
            }
            char[] characters2 = new char[] {'a', 'b', 'c'}; // inicjalizacja tablicy
            char[] characters3 = {'a', 'b', 'c'}; // skrócona notacja
            // Dla tablic z typami wartościowymi wydajność jest mniejsza niż przy typach referencyjnych, ponieważ takie tablice przechowują wszystkie defaultowe wartości, w drugim przypadku są tylko puste referencje

            // Wyróżniamy dwa typy tablic wielowymiarowych (prostokątne|nieregularne)
            int[,] tab = new int[2,2]; // prostokątne deklaruje się oddzielając przecinkami
            // tab.GetLength(1) - długość 2 wymiaru (numeracja od 0)
            int[,] tab2 = new int[,] // inicjalizacja podczas deklaracji
            {
                {2, 4},
                {6, 8}
            };
            int[][] tab3 = new int[2][]; // nieregularne deklaruje się dodaniem odpowiedniej liczby nawiasów kwadratowych
            // defaultowo posiadają wartości null, tablice wewnętrzne nie mają określonej długości, trzeba zainicjalizować
            
            for (int i = 0; i < tab3.Length; i++)
            {
                tab3[i] = new int[2];
                // ...
            }
            
            int[][] tab4 = new int[][]
            {
                new int[] {2, 4},
                new int[] {6, 8}
            };
            // tak jak przy zwykłych typach można i tutaj zastosować słowo kluczowe var
            var characters4 = new[] {'x', 'y', 'z'}; // kompilator ustawi typ char[], uwaga! wszystkie elementy muszą być tego typu
        }
    }
}
