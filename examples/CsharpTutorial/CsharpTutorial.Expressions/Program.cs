﻿using System;

/*
 * W C# kolejność wykonywania ma duże znaczenie - jak w każdym języku programowania, czy samej algebrze
 * Reguły są takie same jak w naukach ścisłych, trzeba pamiętać tylko, że niektóre z operatorów mają łączność z prawej lub z lewej strony
 * Występują wyrażenia przypisania, puste (void) lub podstawowe (przy korzystaniu z podstawowych mechanizmów)
 */

namespace CsharpTutorial.Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(1 + 3 + 5); // łączność lewostronna coś jak (1 + 3) + 5
            int x, y;
            x = y = 5; // łączność prawostronna, najpierw przypisana wartość do y, potem do x

            // Występuje 3 rodzaje operatorów
            // - jednoargumentowe
            // - dwuargumentowe
            // - trójargumentowe
            string emptyText = null;
            string notEmptyText = emptyText ?? "not empty"; // operator sprawdzania ??, sprawdza czy null i przypisuje podaną wartość
            string anotherText = "10";
            string text = anotherText?.ToString(); // operator warunkowy Elvis, wywołuje metody lub przypisuje wartość null
        }
    }
}
