﻿using System; // dyrektywa using użyta do wczytania przestrzeni nazw

/**
 * Każda aplikacja zawiera metodę Main (punkt startowy) występuje w postaci pliku .exe
 * Biblioteka różni się od aplikacji tym, że nie ma zdefiniowanego punktu początkowego, posiada rozszerzenie .dll
 * Kompilator C# kompiluje kod źródłowy (pliki .cs) do postaci zestawów (assemblies)
 * Kompilować można także z konsoli użytkownika: 
 * csc Program.cs -> Program.exe (defaultowo wynikiem jest aplikacja)
 * csc /target:library Program.cs -> Program.dll (wynikiem jest biblioteka)
 * 
 * Kod źródłowy posiada identyfikatory
 * Nazywamy nimi wszelkie nazwy nadane przez programistę
 * Nie mogą zawierać przerw i muszą składać się ze znaków Unicode
 * Rozpoczyna się od podkreślenia lub litery
 * 
 * Standardowa notacja to camelCase (parametry, zmienne, pola prywatne)
 * Cała reszta w notacji PascalCase
 * 
 * Kod źródłowy zawiera słowa kluczowe (często nazywane słowami zarezerwowanymi)
 * Lista słów kluczowych: https://docs.microsoft.com/pl-pl/dotnet/articles/csharp/language-reference/keywords/index
 * Gdy musimy skorzystać z danego słowa kluczowego (a w 99% nie musimy) to możemy użyć obejścia w postaci użycia przedrostku @, np. @static
*/

namespace CsharpTutorial.HelloWorld
{
    class Program
    {
        static void Main(string[] args) // deklaracja metody (wykonuje blok instrukcji)
        {
            Console.WriteLine("Hello World!"); // instrukcja musi być zakończona średnikiem
            Console.Read();
        }
    }
}
