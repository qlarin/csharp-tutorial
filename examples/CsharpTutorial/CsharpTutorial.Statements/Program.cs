﻿using System;

// Instrukcje dzielimy na (wyboru|pętle|warunki)

namespace CsharpTutorial.Statements
{
    class Program
    {
        static void Main(string[] args)
        {
            if (7 > 8 * 2) // instrukcja warunkowa if - unikajmy zbyt wielu instrukcji tego typu, tym bardziej zagnieżdżonych (podatne na błędy, trudne w rozwijaniu)
            {
                Console.WriteLine("No chyba żartujesz");
            } else // blok else - co w ma się wykonać w przypadku gdy warunek nie zostanie spełniony
            {
                Console.WriteLine("To nic nowego");
            }

            int number = 2;
            switch (number) // instrukcja warunkowa switch - słabe w rozwijaniu, mało optymalne
            {
                case 1:
                case 2:
                    Console.WriteLine("Co to?");
                    break; // przejście na koniec
                case -1:
                    Console.WriteLine("O panie!");
                    goto case 1; // przejście do innego case'a (instrukcja skoku goto)
                default: // wszystkie pozostałe
                    Console.WriteLine("Pomyłka");
                    break;
            }

            int i = 0;
            while (i < 10) // instrukcja iteracyjna while, powtarza blok dopóki nie spełniony zostanie warunek
            {
                Console.Write(i + " ");
                if (i++ > 6)
                {
                    break; // instrukcja skoku break - wychodzi z pętli
                }
            }

            do // instrukcja do-while, test warunku wykonuje po bloku instrukcji
            {
                if (i++ > 6)
                {
                    continue; // instrukcja skoku continue - przechodzi do kolejnej iteracji
                }
                Console.Write(i + " ");
            } while (i < 10);

            for (int j = 0; j < 10; j++) // instrukcja for, pętla wypisuje liczby od 0 do 9
            {
                Console.Write(j + " ");
            }
            // wszystkie klauzule instrukcji for jesteśmy w stanie opóścić, co daje nam pętle nieskończoną

            int[] tab = { 1, 2, 3, 4, 5 };
            foreach (int element in tab) // pętla foreach, iteruje po elementach obiektów przeliczalnych, np. tablic, łańcuchów etc.
            {
                Console.Write(element + " ");
            }
        }
    }
}
