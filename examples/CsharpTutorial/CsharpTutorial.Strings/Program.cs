﻿using System;

/**
 * Do typów znakowych należą:
 * - pojedyncze znaki - char
 * - łańcuchy - string
 * Znaki pojedyncze mogą być interpretowane przez specjalne sekwencje (dla znaków Unicode i nie tylko) np. \\ lub (\n || \u000A)
 * Konwersja automatyczna znaków na liczby może nastąpić jedynie w zakresie wartości typu short, inaczej wymagana jest konwersja jawna
 */

namespace CsharpTutorial.Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = "x";
            string y = "y\n"; // można korzystać z sekwencji
            string z = @"\\home\ssledziona\test.cs"; // gdy chcemy zrezygnować z sekwencji, literał dosłowny @
            string xy = "x" + "y"; // konkatenacja, nieefektywna - lepiej skorzystać ze StringBuildera - będzie w późniejszym czasie
            string xz = "x" + 10; // konkatenacja, 2 wartość automatycznie uruchamia metodę ToString()
            Console.WriteLine(x);
        }
    }
}
