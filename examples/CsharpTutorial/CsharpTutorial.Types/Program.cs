﻿using System;

/**
 * Typ - szablon określający wartość
 * Zmienna - miejsce w pamięci gdzie może znajdować się jakaś wartość
 * Wartość - egzemplarz jakiegoś szablonu (typu)
 * https://docs.microsoft.com/en-us/dotnet/articles/csharp/language-reference/keywords/built-in-types-table
 */

namespace CsharpTutorial.Types
{
    class Program
    {
        static void Main(string[] args)
        {
            // Typy predefiniowane
            string text = "Jakiś tekst"; // string, na którym można wykonywać róże funkcje
            int number = 37; // int 32bit
            bool isOk = true; // boolean (true|false)
            if (isOk)
            {
                Console.WriteLine(text + number.ToString());
            } else
            {
                // Typ definiowany przez programistę
                UserSpecialType userSpecialType = new UserSpecialType(number); // utworzenie egzemplarza przy użyciu operatora new
                Console.WriteLine(userSpecialType.ToString());
                Console.WriteLine(UserSpecialType.counter); // przykład operowania na samym typie, bez tworzenia egzemplarza
            }
            Console.ReadLine();

            /*
             * Konwersja typów zawsze zwraca nową wartość
             * Może być jawna lub niejawna
             * Jawne wymagają rzutowania
             * Niejawne wykonują się automatycznie
             * 
             * Występują także inne rodzaje konwersji - będzie o tym mowa w przyszłości
             */
            long longNumber = number; // niejawna konwersja na typ long 64bit (tylko gdy kompilator ma pewność, że operacja się uda i nie dojdzie do utraty danych)
            short shortNumber = (short) number; // jawna konwersja na typ short 16bit (brak pewności, może dojść do utraty danych)
            float floatNumber = number; // niejawna konwersja na typ float 32bity
            number = (int)floatNumber; // wymagana konwersja jawna, należy pamiętać, że rzutując wartości zmiennoprzecinkowe na całkowite może nastąpić utrata precyzji

            /**
             * Podział typów:
             * - referencyjne (klasy, tablice, interfejsy, delegacje i string)
             * - wartościowe (większość typów predefiniowanych, a także zdefiniowane przez programistę (struct|enum)
             * Różnią się głównie sposobem w jaki przechowywana jest wartość
             */

            // utworzenie egzemplarza własnego typu wartościowego
            Value value1 = new Value();
            value1.X = 9;
            value1.Y = 13;
            Value value2 = value1; // tworzona jest kopia egzemplarza
            value1.Y = 10;
            Console.WriteLine(value1.X * value1.Y);
            Console.WriteLine(value2.X * value2.Y);
            // value1 = null; // błąd kompilacji - jest to niemożliwe

            // utworzenie egzemplarza własnego typu referencyjnego
            UserSpecialType type1 = new UserSpecialType(9);
            UserSpecialType type2 = type1; // przypisanie referencji do obiektu type1
            type1.special = 13;
            Console.WriteLine(type1.ToString());
            Console.WriteLine(type2.ToString());
            type1 = null; // gdy przypiszemu null - oznaczać to będzie, że referencja nie wskazuje na żaden obiekt
            Console.WriteLine(type1 == null); // próba wypisania pola dla obiektu type1 spowoduje wyjątek

            // literały liczbowe przedstawić można w różnych notacjach
            int x = 127;
            long y = 0xFF;
            double z = 77.7; // domyślnie notacja kropkowa oznacza typ double, w przeciwnym wypadku int (zależnie od wartości pola), jednakże przy użyciu przyrostków można określić typ literału np. F - float
            double v = 1E07;

            /**
             * Operatory:
             * 
             * - dodawanie (+)
             * - odejmowanie (-)
             * - mnożenie (*)
             * - dzielenie (/)
             * - modulo (%)
             * 
             * Bitowe:
             * - dopełnienie (~)
             * - i (&)
             * - lub (|)
             * - wykluczające (^)
             * - przesunięcia w lewo i w prawo
             * 
             * - inkrementacja (x++) 
             * - dekrementacja (x--)
             * Można stosować wartości przed lub po użyciu inkrementacji|dekrementacji
             * Przed - najpierw zmiana wyniku potem zwracamy wartość (++x)
             * Po - najpierw zwracamy wartość potem zmieniamy wynik (x++)
             * 
             * Dzielenie przez 0 powoduje wyjątek - unikać lub przechwytywać
             * W przypadku przepełnienia następuje zawinięcie wartości
             */
            int x2 = int.MaxValue;
            Console.WriteLine(++x2 == int.MinValue);
            // Jest możliwośc uchronienia się przed zawinięciem przy zastosowaniu operatora checked
            // W takiej sytuacji rzuca wyjątek overflow

            int x3 = int.MaxValue;
            int y3 = checked (++x3); // sprawdza wyrażenie
            // lub:
            checked // sprawdza cały blok
            {
                y3 *= x3;
            }

            // analogicznie unchecked wyłącza sprawdzanie przepełnienia
            // oczywiście przepełnienia liczb zmiennoprzecinkowych nie występują, jako, że tego typu liczby posiadają wartości nieskończoności np. double.PositiveInfinity

            // Typy logiczne:
            // Dostępne operatory logiczne (==, !=, <, >, <=, >=)
            // Dostępne operatory warunkowe (&&, ||, !)

            Console.WriteLine(10 == 100); // porównuje wartości
            UserSpecialType type3 = new UserSpecialType(9);
            UserSpecialType type4 = new UserSpecialType(9);
            Console.WriteLine(type3 == type4); // porównuje referencje

            // Słowo kluczowe var nakazuje kompilatorowi niejawnie określić typ
            var x4 = 3; // kompilator nada typ int
            var y4 = "tekst"; // kompilator nada typ string
        }
    }

    /**
     * Typ posiada dane i funkcje (często nazywane składowymi)
     * W tym przykładzie za dane odpowiada pole
     * A za funkcje konstruktor i metoda ToString()
     */

    class UserSpecialType // suma rozmiarów pól + dodatkowa alokacja
    {
        public int special; // pole
        public static int counter; // pole statyczne, słowo kluczowe public umożliwia na dostęp do pola innym klasom
        public UserSpecialType (int value) // konstruktor
        {
            special = value;
            counter++; // inkrementacja
        }
        public override string ToString() // nadpisana metoda ToString()
        {
            return "Specjalny typ " + special;
        }
    }

    // własna struktura
    struct Value // 32 + 32 bity (8 bajtów) dokładnie
    {
        public int X, Y;
    }
}
