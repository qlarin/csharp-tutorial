﻿using System;

/**
 * Zmienne określają miejsce w pamięci dla danej wartości
 * Możemy wymienić:
 * - zmienne lokalne
 * - parametry (argumenty)
 * - pola (obiektów lub statyczne)
 * - elementy (w tablicach)
 * 
 * Przechowywane są w dwóch różnych strukturach, na stosie lub na stercie.
 * 
 * Na stosie znajdują się zmienne lokalne i parametry
 * Rozmiar stosu jest elastyczny, zwiększa się z każdym wejściem do bloku i zmniejsza po jego wyjściu
 * 
 * Na stercie przechowywane są egzemplarze typów referencyjnych (często nazywane w skrócie obiektami)
 * W czasie trwania programu każdy nowo inicjalizowany obiekt trafia na stertę
 * Gdy utracona zostanie do niego referencja (odwołanie) następuje czyszczenie tej części pamięci przez system nieużytków (garbage collector)
 */

namespace CsharpTutorial.Variables
{
    class Program
    {
        private static int minuend;
        private static int addend;

        static void Main(string[] args)
        {
            int x = 10; // ląduje na stosie, po wyjściu z funkcji zostanie zwolniona pamięć
            AnotherProgram y = new AnotherProgram(); // ląduje na stercie
            // y - nieużywany trafia na do usuwania nieużytków
            Console.WriteLine(AnotherProgram.staticVariable); // przekazane do usuwania dopiero w momencie niszczenia domeny - różni się od zwykłych referencji

            int y2;
            // Console.WriteLine(y2); // musi zostać zainicjalizowana, inaczej błąd kompilacji
            // pola tablicy czy obiektów standardowo mają przypisane wartości domyślne, zależne od typu
            // dla referencji - null
            // dla numerycznych - 0
            // dla znakowych - '\0'
            // dla logicznych - false
            // Standardową wartość sprawdzamy operatorem default
            int x2 = default (int);

            // Argumenty przekazujemy przez wartość (kopia - domyślnie) lub referencję
            int x3 = 3;
            Second(x3);
            Console.WriteLine(x3);
            // Przekazanie argumentu typu referencyjnego przez wartość jedynie kopiuje referencję - tzn. zmiana kopii nie wpływa na obiekt
            // Aby przekazać argument przez referencję należy skorzystać z modyfikatorów ref lub out
            int y3 = 7;
            Third(ref y3);
            Console.WriteLine(y3);
            // Modyfikator out różni się od ref tym, że wymaga by przypisano wartość przed wyjściem z funkcji
            int z = 5;
            Fourth (z, out minuend, out addend); // nie trzeba inicjalizować przed, aczkolwiek zmienna musi być zadeklarowana
            Console.WriteLine(minuend);
            Console.WriteLine(addend);

            int sum = Fifth (1, 2, 3, 5, 7, 11);
            Console.WriteLine(sum);

            // Argumenty jesteśmy w stanie przekazywać po nazwie, kolejność nie ma znaczenia, nie wolno mieszać sposobów przekazywania - błąd kompilacji
            Sixth(y:5, x:3);
        }

        static void Second(int param1, bool param2 = true) // metody mogą zawierać argumenty z wartościami opcjonalnymi, można je wtedy omijać, nie wolno oznaczać operatorami ref lub out
        {
            param1 *= param1;
            if (param2)
            {
                Console.WriteLine(param1);
            }
        }

        static void Third(ref int param3)
        {
            param3 += param3;
            Console.WriteLine(param3);
        }

        static void Fourth(int number, out int minuend, out int addend)
        {
            minuend = number - 2;
            addend = number + 2;
        }

        static int Fifth(params int[] integers) // modyfikator params sygnalizuje dowolną liczbę argumentów
        {
            int result = 0;
            for (int i = 0; i < integers.Length; i++)
            {
                result += integers[i];
            }
            return result;
        }

        static void Sixth(int x, int y)
        {
            Console.WriteLine("To jest x: " + x + " a to y: " + y);
        }
    }

    class AnotherProgram
    {
        public static string staticVariable = "Żyje"; // pola statyczne również lądują na stercie
    }
}
