﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Klasy są najpopularniejszymi typami referencyjnymi
 * 
 * Mogą być prefiksowane odpowiednimi modyfikatorami, np.:
 * - public - dostępne ogólnie
 * - private - dostępne w ramach danej klasy
 * - protected - dostępne w ramach klas dziedziczących
 * - internal - dostępne dla klas tego samego podzespołu
 * - internal protected - dostępne dla klas dziedziczących nawet poza biblioteką, a także klasy z danego podzespołu
 * - static - klasy statyczne nie mogą posiadać obiektów
 * - partial - rozbicie klasy na kilka części w danej bibliotece (tworzą integralną całość, wszystkie części zachowują się tak samo - jesli ktoras jest sealed, to kazda etc.)
 * - sealed - zabronienie na dziedziczenie danej klasy
 * 
 * Mogą być generalizowane - o tym w dalszej części
 */

namespace CsharpTutorial2.Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            First first1 = new First(); // tworzenie egzemplarza klasy First                   
            //  first1.Name = "test2"; // nie jest to możliwe z powodu modyfikatora readonly
            First first2 = new First("test3");
            First first3 = new First("test4", 21);
            // First first4 = new First("test5", 22, 179); // to nie zadziała, konstruktor jest prywatny
            First first5 = First.InitFirst("test5", 22, 179); // utworzenie egzemplarza przy użyciu metody statycznej
            First first6 = new First("test6") { Age = 23, Height = 178 }; // utworzenie egzemplarza przy użyciu inicjalizatora obiektu, to samo jakbyśmy stworzyli obiekt, a potem ustawili pola
            First first7 = new First(first6); // utworzenie obiektu wraz z powiązaniem go z innym

            Another another1 = new Another();
            Console.WriteLine(another1[2]); // wykorzystanie indeksatora do pobrania 3 elementu listy
            another1[4] = "pięć"; // dodanie 5 elementu do listy przy użyciu indeksatora
            Console.WriteLine(another1[4]); // PYTANIE! co tu się zadzieje? :D
        }
    }

    class First
    {
        public const string Message = "Hej nowy!"; // stała, obowiązkowo musi być zainicjalizowana - wartość wyliczana podczas kompilacji
        readonly string Name; // pole tylko do odczytu, można przypisać tylko w deklaracji lub konstruktorze
        public int Age = 20, Height = 180; // inicjalizacja wykonuje się przed wywołaniem konstruktora
        public First Second;
        private Boolean sick; // pole hermetyzowane własnością

        public Boolean Sick // własność
        {
            get { return sick; } // getter kompilowany do get_Sick
            internal set { sick = value; } // setter, value = przekazana wartość
        }

        public Boolean Sick2 { get; set; } = true; // skrócona notacja metod dostępowych get; set; inicjalizowana własność automatyczna

        public Boolean Sick3 => !sick; // własność wyrażeniowa tylko do odczytu

        internal First () // konstruktor, jesli nie ma zdefiniowanego uzyty jest niejawny konstruktor
        {
            Name = "test"; // inicjalizacja
        }

        public First (string name) // przeciążony konstruktor
        {
            Name = name;
        }

        public First (string name, int age) : this (name) // wywołanie dodatkowego konstruktora celem uniknięcia powielania kodu
        {
            Age = age;
        }

        private First (string name, int age, int height) { } // konstruktor prywatny stosowany głównie do kontroli tworzonych obiektów
        public static First InitFirst (string name, int age, int height) // metoda statyczna zwracająca nowy egzemplarz
        {
            return new First(name, age, height);
        }

        public First (First second)
        {
            Second = second; // referencja do przekazanego obiektu
            second.Second = this; // utworzenie referencji w przekazanym obiekcie do konstruowanego egzemplarza
        }

        static First () { Console.WriteLine("Inicjalizacja.."); } // konstruktor statyczny - tylko raz dla typu, czy to przy konstrukcji czy dostępie do składowej

        ~First ()
        {
            Console.WriteLine("Usuwanie..");
        }

        int Method1 (int x) => x * x; // metody mogą być zapisane w formie wyrażeń
        void Method2 (int x) => Console.WriteLine(x); // metody w formie wyrażeniowej mogą zwracać void
        void Method2(int x, int y) => Console.WriteLine(x * y); // metody mogą być przeciążane pod warunkiem posiadania innej sygnatury
       // int Method2(int x) => x * x; // blad kompilacji, zmiana typu zwracanego nic nie da
    }

    class Another
    {
        string[] list = { "jeden", "dwa", "trzy", "cztery" };

        public string this [int idx] // definicja indeksatora dla listy
        {
            get { return list[idx]; }
            set { list[idx] = value; }
        }
    }
}
