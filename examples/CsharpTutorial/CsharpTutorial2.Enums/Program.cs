﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Wyliczeń stosuje się głównie do definiowania grup stałych liczb, np statusów etc.
 * Każda składowa wyliczenia posiada wartości typu int
 * Wartości przypisywane są niejawnie, automatycznie
 * Przy przypisaniu własnej wartości inkrementacja jest przeprowadzana od tej właśnie wartości
 */

namespace CsharpTutorial2.Enums
{
    class Program
    {
        public enum Status { New, Accepted, Rejected, Done };

        static void Main(string[] args)
        {
            Status status = Status.New;
            Console.WriteLine(status);
            Console.WriteLine(status.ToString());
            Console.WriteLine(status < Status.Rejected);
            Console.WriteLine(++status);
            Console.ReadLine();
        }
    }
}
