﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Typy generyczne są po dziedziczeniu kolejnym mechanizmem pomagającym w pisaniu kodu dla różnych typów
 * Służą do tworzenia schematów działających z różnymi typami danych
 * Zwiększają bezpieczeństwo jeśli chodzi o typowanie
 * Redukują liczbę operacji rzutowania czy pakowania
 */

namespace CsharpTutorial2.Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            var warehouse = new Warehouse<Weapon>(); // inicjalizacja magazynu na bronie
            warehouse.Push(new Weapon() { Level = 13 });
            Console.WriteLine(warehouse.Pop());
            string x = "Green";
            string y = "Red";
            Swap(ref x, ref y);
            Console.WriteLine(x + " " + y);
            Console.ReadLine();
        }

        static void Swap<T> (ref T x, ref T y) // metoda generyczna do zamiany dwóch wartości
        {
            T temp = x;
            x = y;
            y = temp;
        }
    }

    public class Weapon
    {
        public int Level { get; set; }

        public override string ToString()
        {
            return base.ToString() + " level: " + Level.ToString();
        }
    }

    public class Warehouse<T> where T : Weapon // klasa generyczna magazynu, ktory moze przechowywac przedmioty danego typu T, z ograniczeniem do typu Weapon lub jego pochodnych
    {
        int position;
        T[] items = new T[10]; // powiedzmy, że magazyn przechowuje 10 elementow danego typu
        public void Push(T item) => items[position++] = item;
        public T Pop() => items[--position];
    }
}
