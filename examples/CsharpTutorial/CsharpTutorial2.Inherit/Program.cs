﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 *  Dziedziczenie - rozszerzanie klasy celem dostosowania jej do jakichś potrzeb
 *  - klasa może dziedziczyć tylko po jednej klasie
 *  - jedna klasa może być wykorzystana do dziedziczenia przez wiele klas
 */

namespace CsharpTutorial2.Inherit
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal = new Lion(); // zastosowanie polimorfizmu
            animal.WhoAmI();
            Console.ReadLine();
            Lion lion = new Lion();
            Animal animal2 = lion; // rzutowanie w górę do klasy bazowej - zawsze pomyslnie (odwoluje sie do tego samego miejsca w pamieci ale ma bardziej ograniczony dostep)
            animal2.WhoAmI();
            Console.ReadLine();
            //  Console.WriteLine(animal2.Race); // błąd kompilacji
            Animal animal3 = (Lion)animal2; // rzutowanie w dół - w razie niepowodzenia wyjatek
            Animal animal4 = animal2 as Lion; // rzutowanie w dół operatorem as, w razie niepowodzenia null
            if (animal2 is Lion) // operator is sprawdza czy rzutowanie sie powiedzie
            {
                ((Lion)animal2).WhoAmI();
            }

            // wszystkie typy reprezentowane są przez System.Type, można go otrzymać korzystając z metod:
            // GetType - w czasie działania
            // typeof - w czasie kompilacji
            Console.WriteLine(animal2.GetType().Name); // Lion
            Console.WriteLine(typeof (Lion).Name); // Lion
            //  Console.WriteLine(typeof (animal2).Name); // blad kompilacji - tak nie mozna
            // Pozostałe metody z klasy Object stosuje się głównie do badania równości wartości lub referencji
        }
    }

    class Animal : Creature
    {
        public sealed override string Kind { get; set; } = "animal"; // zablokowanie nadpisywania w klasach potomnych

        public Animal ()
        {
            Console.WriteLine("Test animal");
        }

        public virtual void WhoAmI () // virtual umożliwia nadpisywanie przez klasy potomne
        {
            Console.WriteLine("I am a kind of " + Kind);
        }
    }

    class Lion : Animal // klasa pochodna dziedziczy po klasie Animal
    {
        public string Race { get; set; } = "lion";

        public Lion () : base () // wywołanie konstruktora klasy bazowej
        {
            Console.WriteLine("Test lion");
        }

        public override void WhoAmI () // nadpisywanie metody z nadklasy
        {
            Console.WriteLine("I am a " + Race + " - " + Kind + "'s king");
        }
    }

    abstract class Creature // klasa abstrakcyjna, nie można tworzyć jej egzemplarzy
    {
        public abstract string Kind { get; set; }
    }
}
