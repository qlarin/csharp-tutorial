﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Interfejs jest to zbiór specyfikacji, które powinna zawierać klasa
 * - wszystkie jego składowe są defaultowo abstrakcyjne
 * - pomimo tego, można tworzyć składowe konkretne
 * - klasa lub struktura mogą implementować wiele interfejsów
 * - zawiera tylko metody, własności, indeksatory i zdarzenia
 * - składowe interfejsu są zawsze publiczne
 * 
 * Kiedy wybrać interfejs a kiedy klasę?
 * - interfejs lepiej zastosować przy niezależnych implementacjach
 * - klasy i podklasy gdy mają wspólne części
 * 
 * np. umiejętności latania lepiej zaimplementować jako interfejs
 * ale to czy obiekt jest ptakiem lepiej opisać klasą
 */ 

namespace CsharpTutorial2.Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            ICounter counter = new Counter();
            while (counter.Next())
            {
                Console.WriteLine(counter.Current);
            }
            Console.ReadLine();
        }
    }

    public interface ICounter
    {
        bool Next();
        object Current { get; }
    }

    public class Counter : ICounter // implementacja interfejsu, trzeba konkretyzować metodę Next() i składową Current
    {
        int Count = 0;
        bool ICounter.Next() => Count++ < 10;
        object ICounter.Current => Count; // jawna implementacja umożliwia uniknięcie kolizji sygnatur z różnych interfejsów
    }
}
