﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial2.Nested
{
    class Program
    {
        private static int x; // pole jest prywatne, mimo to istnieje do niego dostęp wewnątrz

        static void Main(string[] args)
        {
            Nested.Write();
            Console.ReadLine();
        }

        class Nested // często stosowane do generowania klas przechowujących iteratory etc.
        {
            public static void Write()
            {
                Console.WriteLine(++Program.x); // zagnieżdżona klasa ma dostęp do nadrzędnej klasy
            }
        }
    }
}
