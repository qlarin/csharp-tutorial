﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Klas częściowych używamy do podzielenia definicji na mniejsze części
 * - każda część musi posiadać w deklaracji słowo kluczowe partial
 * - części nie mogą posiadać konstruktorów przyjmujących te same parametry
 * - każda część musi być dostępna podczas kompilacji
 * - wszystkie części muszą znajdować się w tym samym zestawie
 * - nie ma zasady co do kolejności wykonywania inicjalizacji w klasach częściowych
 */

namespace CsharpTutorial2.PartialClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            PartialOne partialOne = new PartialOne();
            Console.WriteLine(partialOne.test1 + ' ' + partialOne.test2);
        }
    }

    partial class PartialOne // klasa częściowa z implementacją
    {
        public string test1 = "test1";

        partial void PartialMessage()
        {
            Console.WriteLine("Inicjalizacja partial");
        }
    }

    partial class PartialOne // klasa częściowa bez implementacji - muszą mieć taką samą nazwę
    {
        public string test2 = "test2";

        partial void PartialMessage();
    }
}
