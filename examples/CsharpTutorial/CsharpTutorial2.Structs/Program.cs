﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * Struktura jest podobna do klasy
 * Różnice:
 * - jest typem wartościowym
 * - nie może dziedziczyć po innych strukturach
 * - nie może mieć konstruktora bezparametrowego
 * - nie posiada finalizatora
 * - nie posiada inicjalizatorów
 * - nie posiada składowych wirtualnych i chronionych
 */

namespace CsharpTutorial2.Structs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine((new Point3D(3, 7, 5)).x);
            Console.ReadLine();
        }
    }

    public struct Point3D
    {
        public int x, y, z; // nie można inicjalizować
        public Point3D (int x, int y, int z) // trzeba ustawić wszystkie własności
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
