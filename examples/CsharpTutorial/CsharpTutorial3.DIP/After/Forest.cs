﻿using CsharpTutorial3.DIP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP.After
{
    class Forest
    {
        IWalkable creature;

        public Forest()
        {
            Console.WriteLine("You have wake up in dangerous forest..");
        }

        public void Spawning(IWalkable c)
        {
            creature = c;
            creature.Walk();
        }
    }
}
