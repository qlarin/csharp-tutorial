﻿using CsharpTutorial3.DIP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP.After
{
    class Goblin : IWalkable
    {
        public void Walk()
        {
            Console.WriteLine("Goblin patrols the area..");
        }
    }
}
