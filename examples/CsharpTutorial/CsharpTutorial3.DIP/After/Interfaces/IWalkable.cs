﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP.After.Interfaces
{
    interface IWalkable
    {
        void Walk();
    }
}
