﻿using CsharpTutorial3.DIP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP.After
{
    class Wolf : IWalkable
    {
        public void Walk()
        {
            Console.WriteLine("Wolf is walking around..");
        }
    }
}
