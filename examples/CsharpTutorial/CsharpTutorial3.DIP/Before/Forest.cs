﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP.Before
{
    class Forest
    {
        public Forest()
        {
            Console.WriteLine("You have wake up in dangerous forest..");
        }

        public void Spawning(Object c)
        {
            if (c is Wolf)
            {
                Wolf creature = c as Wolf;
                creature.Walk();
            } else if (c is Goblin)
            {
                Goblin creature = c as Goblin;
                creature.Walk();
            }
        }
    }
}
