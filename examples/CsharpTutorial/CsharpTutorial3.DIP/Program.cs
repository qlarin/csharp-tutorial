﻿using CsharpTutorial3.DIP.After;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.DIP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            Before.Forest dangerousForest = new Before.Forest();
            dangerousForest.Spawning(new Before.Goblin());
            dangerousForest.Spawning(new Before.Wolf());
            Console.ReadLine();

            // Dependency Inversion Principle
            // Wszelkie zależności powinny zależeć głównie od abstrakcji, nie od konkretnych typów

            // After
            Forest forest = new Forest();
            forest.Spawning(new Goblin());
            forest.Spawning(new Wolf());
            Console.ReadLine();
        }
    }
}
