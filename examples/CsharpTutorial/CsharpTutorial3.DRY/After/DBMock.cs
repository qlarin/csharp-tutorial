﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.Metaprinciples.After
{
    class DBMock<T>
    {
        List<T> list = new List<T>();

        public List<T> GetAll()
        {
            return list;
        }

        public void Add(T item)
        {
            list.Add(item);
        }
    }
}
