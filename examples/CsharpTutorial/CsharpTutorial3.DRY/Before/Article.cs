﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.Metaprinciples.Before
{
    class Article
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public Article(string title, string desc)
        {
            Title = title;
            Description = desc;
        }

        public override string ToString()
        {
            return Title + " " + Description;
        }
    }
}
