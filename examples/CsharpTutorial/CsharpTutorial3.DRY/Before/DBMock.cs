﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.Metaprinciples.Before
{
    class DBMock
    {
        public List<Person> GetPeople()
        {
            Person person1 = new Person("Adam", "Nowak");
            Person person2 = new Person("Marta", "Nowak");
            List<Person> list = new List<Person>();
            list.Add(person1);
            list.Add(person2);
            return list;
        }

        public List<Article> GetArticles()
        {
            Article article1 = new Article("Pierwszy", "Treść..");
            Article article2 = new Article("Drugi", "Treść 22..");
            List<Article> list = new List<Article>();
            list.Add(article1);
            list.Add(article2);
            return list;
        }
    }
}
