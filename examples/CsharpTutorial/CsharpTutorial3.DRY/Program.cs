﻿using CsharpTutorial3.Metaprinciples.Before;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.Metaprinciples
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            DBMock db = new DBMock();
            foreach (var item in db.GetPeople())
            {
                Console.WriteLine(item.ToString());
            }
            foreach (var item in db.GetArticles())
            {
                Console.WriteLine(item.ToString());
            }
            Console.ReadLine();

            // After
            After.DBMock<Person> personDB = new After.DBMock<Person>();
            Person person1 = new Person("Adam", "Nowak");
            Person person2 = new Person("Marta", "Nowak");
            personDB.Add(person1);
            personDB.Add(person2);
            foreach (var item in personDB.GetAll())
            {
                Console.WriteLine(item.ToString());
            }
            Console.ReadLine();

            // What's more?
            // KISS https://en.wikipedia.org/wiki/KISS_principle
            // YAGNI https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it
            // LOD https://en.wikipedia.org/wiki/Law_of_Demeter
            // and many many more

            // ... stay agile
            // analize and measure your code
            // http://www.c-sharpcorner.com/article/measure-your-code-using-code-metrics/

            // Bad code does too much
            // Avoid redundancy
            // Reading your code should be fast and easy
            // Less code = better code
            // Code should be easily extendable by another devs
            // Test your code
        }
    }
}
