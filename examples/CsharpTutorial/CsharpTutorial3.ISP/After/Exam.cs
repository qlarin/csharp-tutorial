﻿using CsharpTutorial3.ISP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.ISP.After
{
    class Exam : IPrintablePdf, IPrintableDocx
    {
        public void PrintDocx()
        {
            Console.WriteLine("printing docx exam ..");
        }

        public void PrintPdf()
        {
            Console.WriteLine("printing pdf exam ..");
        }
    }
}
