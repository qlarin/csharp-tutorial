﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.ISP.After.Interfaces
{
    interface IPrintableDocx
    {
        void PrintDocx();
    }
}
