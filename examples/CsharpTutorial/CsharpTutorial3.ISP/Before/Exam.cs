﻿using CsharpTutorial3.ISP.Before.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.ISP.Before
{
    class Exam : IPrintable
    {
        public void PrintDocx()
        {
            Console.WriteLine("printing docx exam ..");
        }

        public void PrintPdf()
        {
            Console.WriteLine("printing pdf exam ..");
        }
    }
}
