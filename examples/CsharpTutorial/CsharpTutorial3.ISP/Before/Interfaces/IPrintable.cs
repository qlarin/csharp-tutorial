﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.ISP.Before.Interfaces
{
    interface IPrintable
    {
        void PrintPdf();
        void PrintDocx();
    }
}
