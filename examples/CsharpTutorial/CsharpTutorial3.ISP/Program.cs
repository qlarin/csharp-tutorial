﻿using CsharpTutorial3.ISP.Before;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.ISP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            Exam exam = new Exam();
            exam.PrintPdf();
            exam.PrintDocx();

            Invoice invoice = new Invoice();
            invoice.PrintPdf();
            invoice.PrintDocx();

            Console.ReadLine();

            // Interface Segregation Principle
            // Interfejs powinien zawierać jak najmniej definicji
            // Tak by korzystające z niego klasy nie implementowały metod, których nie potrzebują

            // After
            After.Exam exam2 = new After.Exam();
            exam2.PrintPdf();
            exam2.PrintDocx();

            After.Invoice invoice2 = new After.Invoice();
            invoice2.PrintPdf();

            Console.ReadLine();
        }
    }
}
