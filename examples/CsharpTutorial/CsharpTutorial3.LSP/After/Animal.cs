﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.After
{
    abstract class Animal
    {
        public string Name { get; set; }
        public abstract void Move();
    }
}
