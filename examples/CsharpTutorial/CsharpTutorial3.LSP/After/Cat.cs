﻿using CsharpTutorial3.LSP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.After
{
    class Cat : Animal, IWalkable
    {
        public Cat(string name)
        {
            Name = name;
        }

        public override void Move()
        {
            Walk();
        }

        public void Walk()
        {
            Console.WriteLine(Name + " can walk");
        }
    }
}
