﻿using CsharpTutorial3.LSP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.After
{
    class Eagle : Animal, IFlyable
    {
        public Eagle(string name)
        {
            Name = name;
        }

        public override void Move()
        {
            Fly();
        }

        public void Fly()
        {
            Console.WriteLine(Name + " can fly");
        }
    }
}
