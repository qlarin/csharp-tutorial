﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.After.Interfaces
{
    interface IFlyable
    {
        void Fly();
    }
}
