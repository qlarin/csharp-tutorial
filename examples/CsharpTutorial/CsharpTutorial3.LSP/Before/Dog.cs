﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.Before
{
    class Dog : Animal
    {
        public Dog(string name)
        {
            Name = name;
        }

        public override void Walk()
        {
            Console.WriteLine(Name + " walk around you");
        }
    }
}
