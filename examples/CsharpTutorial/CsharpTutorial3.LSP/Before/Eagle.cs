﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP.Before
{
    class Eagle : Animal
    {
        public Eagle(string name)
        {
            Name = name;
        }

        public override void Walk()
        {
            // hmm, I did not want to achieve that behaviour,
            // something is really wrong
            throw new NotImplementedException();
        }
    }
}
