﻿using CsharpTutorial3.LSP.Before;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.LSP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            List<Animal> animals = new List<Animal>();
            animals.Add(new Cat("mark"));
            animals.Add(new Eagle("hawkins"));

            animals.ForEach(animal => animal.Walk());
            Console.ReadLine();

            // Liskov Substitution Principle
            // Każda klasa pochodna powinna być w pełni zgodna z klasą macierzystą

            // After
            List<After.Animal> animals2 = new List<After.Animal>();
            animals2.Add(new After.Cat("clane"));
            animals2.Add(new After.Eagle("shade"));

            animals2.ForEach(animal2 => animal2.Move());
            Console.ReadLine();
        }
    }
}
