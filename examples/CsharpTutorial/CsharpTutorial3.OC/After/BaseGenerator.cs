﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.OC.After
{
    abstract class BaseGenerator
    {
        public abstract void Generate(string content);
    }
}
