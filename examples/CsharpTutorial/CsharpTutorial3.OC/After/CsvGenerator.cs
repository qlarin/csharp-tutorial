﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.OC.After
{
    class CsvGenerator : BaseGenerator
    {
        public override void Generate(string content)
        {
            Console.WriteLine("Generating csv results.." + content);
        }
    }
}
