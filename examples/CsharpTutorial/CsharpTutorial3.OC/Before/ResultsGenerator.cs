﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.OC.Before
{
    class ResultsGenerator
    {
        public ResultsGenerator(string results, string format = "pdf")
        {
            if (format == "pdf")
            {
                Console.WriteLine("Generating pdf results.." + results);
            } else if (format == "csv")
            {
                Console.WriteLine("Generating csv results.." + results);
            }
        }
    }
}
