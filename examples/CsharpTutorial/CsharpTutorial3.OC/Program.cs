﻿using CsharpTutorial3.OC.Before;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.OC
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            Before.ResultsGenerator generator = new Before.ResultsGenerator("very important data", "csv");
            Console.ReadLine();

            // Open/Closed Principle
            // Każda klasa powinna być otwarta na rozbudowę lecz zamknięta na wszelkie modyfikacje

            // After
            After.BaseGenerator generator2 = new After.PdfGenerator();
            generator2.Generate("no more data");
            Console.ReadLine();
        }
    }
}
