﻿namespace CsharpTutorial3.SRP.After
{
    public class Address
    {
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Street { get; set; }
    }
}