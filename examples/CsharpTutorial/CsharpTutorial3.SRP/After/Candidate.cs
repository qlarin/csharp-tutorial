﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.SRP.After
{
    class Candidate
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Address CandidateAddress { get; set; }

        public Candidate(string name, string surname, int age, string phone, string email)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Phone = phone;
            Email = email;
        }

        override public string ToString()
        {
            return Name + " " + Surname;
        }
    }
}
