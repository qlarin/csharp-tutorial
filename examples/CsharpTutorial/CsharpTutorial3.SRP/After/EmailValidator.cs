﻿using CsharpTutorial3.SRP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.SRP.After
{
    class EmailValidator : IValidator
    {
        public bool IsValid(string email) => email.Contains("@");
    }
}
