﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.SRP.After.Interfaces
{
    interface IValidator
    {
        bool IsValid(string name);
    }
}
