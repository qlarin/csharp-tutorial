﻿using CsharpTutorial3.SRP.After.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTutorial3.SRP.After
{
    class PhoneValidator : IValidator
    {
        public bool IsValid(string phone)
        {
            return phone.Length == 9 && phone.All(char.IsDigit);
        }
    }
}
