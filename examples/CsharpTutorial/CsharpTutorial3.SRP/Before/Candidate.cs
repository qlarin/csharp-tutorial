﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsharpTutorial3.SRP.After;

namespace CsharpTutorial3.SRP.Before
{
    class Candidate
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Street { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public Candidate(string name, string surname, int age, string phone, string email)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Phone = ValidatePhone(phone);
            Email = ValidateEmail(email);
        }

        private string ValidatePhone(string phone)
        {
            if (phone.Length != 9 || !phone.All(char.IsDigit))
            {
                throw new Exception("Invalid phone number!");
            }
            return phone;
        }

        private string ValidateEmail(string email)
        {
            if (!email.Contains("@"))
            {
                throw new Exception("Invalid email");
            }
            return email;
        }

        override public string ToString()
        {
            return Name + " " + Surname;
        }
    }
}
