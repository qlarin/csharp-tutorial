﻿using CsharpTutorial3.SRP.Before;
using CsharpTutorial3.SRP.After;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace CsharpTutorial3.SRP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Before
            Before.Candidate candidate = new Before.Candidate("Adam", "Nowak", 19, "900900900", "anowak@gmail.com");
            Console.WriteLine("Utworzono kandydata: " + candidate.ToString());
            Console.ReadLine();

            // Single Responsibility Principle
            // Każda klasa powinna być odpowiedzialna tylko za jeden element (jedną czynność)

            // After
            After.Candidate candidate2 = new After.Candidate("Adam", "Nowakovsky", 21, "500800900", "anovakowsky@gmail.com");
            PhoneValidator phoneValidator = new PhoneValidator();
            EmailValidator emailValidator = new EmailValidator();

            if (!phoneValidator.IsValid(candidate2.Phone))
            {
                throw new Exception("Invalid phone number!");
            }
            if (!emailValidator.IsValid(candidate2.Email))
            {
                throw new Exception("Invalid email!");
            }
            Console.WriteLine("Utworzono kandydata: " + candidate2.ToString());
            Console.ReadLine();
        }
    }
}
